// hooks/useWorker.ts
import { useState, useEffect } from 'react';

interface UseWorkerReturn<T> {
  sendMessage: (message: string) => void;
  response: T | undefined;
}

function useWorker<T>(initMessage?: string): UseWorkerReturn<T> {
  const [response, setResponse] = useState<T>();

  useEffect(() => {
    if (typeof window === 'undefined') return;

    const worker = window.worker;
    const handleMessage = (event: MessageEvent) => {
      console.log(event.data,initMessage);
      
      if (event.data === initMessage) {
        setResponse(event.data);
      }
    };
    worker.addEventListener('message', handleMessage);
    return () => {
      worker.removeEventListener('message', handleMessage);
    };
  }, [initMessage]);

  const sendMessage = (message: string) => {
    if (typeof window === 'undefined') return;
    const worker = window.worker;
    console.log('logged')
    worker.postMessage(message);
  };

  return { sendMessage, response };
}

export default useWorker;
