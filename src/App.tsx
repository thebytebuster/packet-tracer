
import { Layer, Stage } from 'react-konva'
import './App.css'
import RenderPoints from './components/ui/points'
import LinkCanvas from './components/ui/link'
import useDrawStore from './store/draw.store'
import { SheetCanvas } from './components/features/sheetCanvas'
import { useEffect } from 'react'
function App() {
  const points = useDrawStore().points;
  const lines = useDrawStore().lines;
  useEffect(()=>{
    console.log("points",points)
  },[points])
  return (
        <>
          <Stage
            width={window.innerWidth}
            height={window.innerHeight}
          >
            <Layer>
                <LinkCanvas lines={lines} points={points}/>
                <RenderPoints points={points}/>
            </Layer>
        </Stage>
        <div className='absolute bottom-5 left-5' >
            <SheetCanvas/>
          </div>

        </>
      )
}

export default App