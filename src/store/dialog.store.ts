import { DialogType } from "@/type/server";
import { create } from "zustand";
import { combine } from "zustand/middleware";



export const useDialogStore = create (
    combine(
        {
            dialog : {
                type:DialogType.CREATESERVER,
                status:false
            }
        },
        (set)=>(
            {
                toogleDialog : (type:DialogType,status:boolean)=>set({
                    dialog:{type,status}
                })
            }
        )
    )
)