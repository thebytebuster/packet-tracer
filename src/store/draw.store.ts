import { ILine, IPoint } from '@/type/draw';
import { create } from 'zustand'



interface StoreState {
  points: IPoint[];
  lines: ILine[];
  addPoint: (point: IPoint) => void;
  addLine: (line: ILine) => void;
}

const cmToPx = (cm: number) => cm * 20.7953;

const useDrawStore = create<StoreState>((set) => ({
  points: [
    { x: 50, y: 50, label: 'A' },
    { x: 50 + cmToPx(10), y: 50, label: 'B' },
    { x: 50 + cmToPx(20), y: 50, label: 'C' },
    { x: 50 + cmToPx(35), y: 50, label: 'D' },
  ],
  lines: [
    { type: 'line', start: 0, end: 1 },
    { type: 'curve', start: 1, end: 2, depth: cmToPx(2) },
    { type: 'curve', start: 2, end: 3, depth: cmToPx(2), offset: cmToPx(3) },
  ],
  addPoint: (point) => set((state) => ({ points: [...state.points, point] })),
  addLine: (line) => set((state) => ({ lines: [...state.lines, line] })),
}));

export default useDrawStore;