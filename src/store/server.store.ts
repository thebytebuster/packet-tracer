import {IServer ,IConnector, INeighbor} from "@/type/server";
import { Vector2d } from "konva/lib/types";
import { create } from "zustand";
import { combine } from "zustand/middleware";


const initialState : IServer[] =  [
    {
      "domaine": "www.example.com",
      "ip": "192.168.1.100",
      "neighbor": [
        // { "ip": "192.168.1.102", "ping": 12 },
        // { "ip": "192.168.1.104", "ping": 13 },
        // { "ip": "192.168.1.103", "ping": 14 },
        // { "ip": "192.168.1.105", "ping": 15 },
        // { "ip": "192.168.1.101", "ping": 16 }
      ],
      "position": { "x": 555.9456760388634, "y": 236.35393767596318 },
      "srcImage": "images/server_on.svg",
      "status": true
    },
    {
      "domaine": "www.facebook.com",
      "ip": "192.168.1.101",
      "neighbor": [
        // { "ip": "192.168.1.105", "ping": 12 },
        // { "ip": "192.168.1.100", "ping": 16 },
        // { "ip": "192.168.1.102", "ping": 12 }
      ],
      "position": { "x": 17.892293907210007, "y": 238.80985692790404 },
      "srcImage": "images/server_on.svg",
      "status": true
    },
    {
      "domaine": "www.youbute.com",
      "ip": "192.168.1.102",
      "neighbor": [
        // { "ip": "192.168.1.105", "ping": 12 },
        // { "ip": "192.168.1.100", "ping": 12 },
      ],
      "position": { "x": 461.2572919496959, "y": 110.70514419708135 },
      "srcImage": "images/server_on.svg",
      "status": true
    },
    {
      "domaine": "www.twetter.com",
      "ip": "192.168.1.103",
      "neighbor": [
        // { "ip": "192.168.1.104", "ping": 104 },
        // { "ip": "192.168.1.100", "ping": 14 },
        // { "ip": "192.168.1.105", "ping": 103 }
      ],
      "position": { "x": 599.3208275197175, "y": 424.4071897359512 },
      "srcImage": "images/server_on.svg",
      "status": true
    },
    {
      "domaine": "www.wikipedia.com",
      "ip": "192.168.1.104",
      "neighbor": [
        // { "ip": "192.168.1.103", "ping": 104 },
        // { "ip": "192.168.1.102", "ping": 102 },
        // { "ip": "192.168.1.100", "ping": 12 }
      ],
      "position": { "x": 717.8150156930411, "y": 180.08618962172974 },
      "srcImage": "images/server_on.svg",
      "status": true
    },
    {
      "domaine": "www.amazon.net",
      "ip": "192.168.1.105",
      "neighbor": [
        // { "ip": "192.168.1.103", "ping": 103 },
        // { "ip": "192.168.1.100", "ping": 15 },
        // { "ip": "192.168.1.101", "ping": 12 }
      ],
      "position": { "x": 96.32175206727165, "y": 518.3966139907773 },
      "srcImage": "images/server_on.svg",
      "status": true
    }
  ]
  ;
  function mapToConnectors(initialState: IServer[]): IConnector[] {
    const connectors : IConnector[] = []
    initialState.forEach((state)=>{
      state.neighbor.forEach((neighbor)=>{
          const to = initialState.filter((init)=>init.ip==neighbor.ip)[0]
          connectors.push({
            from:{
              x:state.position.x,
              y:state.position.y,
              id:state.ip
            },
              to:{
                x:to.position.x,
                y:to.position.y,
                id:to.ip
              },
              id:state.ip+" "+to.ip,
              ping :neighbor.ping
          })
      })    
    })
    return connectors;
  }
export const useServerStore = create(
    combine(
        {
            servers:[...initialState],
            currentServer: {} as IServer,
            connectors:mapToConnectors(initialState)
        },
        (set) => ({
            removeAllServers:()=>set({servers:[]}),
            addServer:(newServer:IServer)=>set({servers:[...initialState,newServer]}),
            updateServer : (newServer:IServer,ip:string) =>set((state)=>({
                servers : state.servers.map((server)=>{
                    if(server.ip == ip){
                        return newServer
                    }else{
                        return server
                    }
                })
            })),
            setCurrent : (newCurrentSever:IServer)=>set(
                {
                    currentServer : newCurrentSever
                }
            ),
            setConnectors : (args:{pos:Vector2d,ip:string}) => set((state)=>(
                {
                    connectors : state.connectors.map((conn)=>{
                        if(conn.from.id==args.ip){
                            const data:IConnector = {
                              ...conn,
                              from:{
                                x:args.pos.x,
                                y:args.pos.y,
                                id:args.ip
                              }
                            }
                            return data
                          }      
                          if(conn.to.id==args.ip){
                            const data:IConnector = {
                              ...conn,
                              to:{
                                x:args.pos.x,
                                y:args.pos.y,
                                id:args.ip
                              }
                            }
                            return data
                          }
                          return conn
                    })
                })
            ),
            addConnector : (newConnector : IConnector,ping:number) => set((state)=>(
                {
                    connectors : [...state.connectors,newConnector],
                    servers: state.servers.map((conn)=>{
                        if(conn.ip==newConnector.from.id){
                            const neighbor : INeighbor[] = [
                                ...conn.neighbor,
                                {
                                    ip:newConnector.to.id,
                                    ping:ping
                                }
                            ]       
                            return {
                                ...conn,
                                neighbor
                            }
                        }
                        if(conn.ip==newConnector.to.id){
                            const neighbor : INeighbor[] = [
                                ...conn.neighbor,
                                {
                                    ip:newConnector.from.id,
                                    ping:ping
                                }
                            ]       
                            return {
                                ...conn,
                                neighbor
                            }
                        }
                        return conn
                    })
                }
            ))
          })
    )
)