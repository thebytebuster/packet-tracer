import { create } from "zustand";
import { combine } from "zustand/middleware";

export const useEdgeStore = create(
    combine (
        {
            edges : []
        },
        (set) => (
            {
                addEdges : ()=> set({edges:[]})
            }
        )
    )
)