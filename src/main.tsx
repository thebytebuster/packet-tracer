import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.tsx'
import './index.css'
declare global {
  interface Window {
    worker: Worker;
  }
}

const worker = new Worker(new URL('./worker.ts', import.meta.url));
console.log(new URL('./worker.ts', import.meta.url))

if (typeof window!== 'undefined') {
  window.worker = worker;
}

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
)
