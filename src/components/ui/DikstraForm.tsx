import { Select, SelectContent, SelectGroup, SelectItem, SelectTrigger, SelectValue } from "../ui/select"
import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import { z } from "zod";
import { FormField,Form} from "../ui/form";
import { useServerStore } from "@/store/server.store"
import { Button } from "./button";
const formSchema = z.object({
    source: z.string().min(2, {
      message: "",
    }),
    destination : z.string().min(2,{
        message:''
    })
  })
interface dikstraformProps {
    open: boolean;
    onClose: () => void;
}
export default function DikstraForm(props:dikstraformProps) {
    const {servers} = useServerStore();
    const form = useForm<z.infer<typeof formSchema>>({
          resolver: zodResolver(formSchema),
          defaultValues: {
              destination: "",
              source:''
          },
        });
    function onSubmit(values: z.infer<typeof formSchema>) {
        console.log('values',values);
    }
  return (
    <Form {...form}>

    <form onSubmit={form.handleSubmit(onSubmit)} className="flex  flex-col gap-3" >
        <FormField
              control={form.control}
              name="source"
              render={({ field }) => (
                <Select   {...field} onValueChange={(value)=>field.onChange(value)}>
                  <SelectTrigger className="w-full">
                    <SelectValue placeholder="Destination" className="text-sm" />
                  </SelectTrigger>
                  <SelectContent>
                    <SelectGroup>
                      {
                        servers.map((D,idx)=>{
                          return  <SelectItem  key={idx} value={D.ip as string}>{D.domaine}</SelectItem>
                        })
                      }
                    </SelectGroup>
                  </SelectContent>
              </Select>
            )}
          />
          <FormField
            control={form.control}
            name="destination"
            render={({ field }) => (
              <Select   {...field} onValueChange={(value)=>field.onChange(value)}>
                <SelectTrigger className="w-full">
                  <SelectValue placeholder="Source" className="text-sm" />
                </SelectTrigger>
                <SelectContent>
                  <SelectGroup>
                  {
                        servers.map((D,idx)=>{
                          return  <SelectItem  key={idx} value={D.ip as string}>{D.domaine}</SelectItem>
                        })
                      }
                  </SelectGroup>
                </SelectContent>
          </Select>
            )}
          />
           <Button type="submit" className="my-2">Save changes</Button>
    </form>
  </Form>
  )
}
