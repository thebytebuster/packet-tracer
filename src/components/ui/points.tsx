import React from 'react';
import { Text, Circle, Group } from 'react-konva';

interface Point {
  x: number;
  y: number;
  label: string;
}

interface RenderPointsProps {
  points: Point[];
}

const RenderPoints: React.FC<RenderPointsProps> = ({ points }) => {
  return (
      <>
        {points.map((point, index) => (
          <Group key={index}>
            <Circle x={point.x} y={point.y} radius={5} fill="red" />
            <Text x={point.x + 5} y={point.y - 5} text={point.label} />
          </Group>
        ))}
      </>
  );
};

export default RenderPoints;