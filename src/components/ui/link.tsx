import { ILine, IPoint } from '@/type/draw';
import React from 'react';
import { Line } from 'react-konva';

interface LinkCanvasProps {
  points: IPoint[];
  lines: ILine[];
}

const drawCurve = (start: IPoint, end: IPoint, depth: number, offset: number = 0) => {
  if (offset) {
    return [start.x, start.y, end.x - offset, end.y - depth, end.x, end.y];
  }
  const midX = (start.x + end.x) / 2;
  const midY = (start.y + end.y) / 2;
  return [start.x, start.y, midX, midY - depth, end.x, end.y];
};

const drawLine = (start: IPoint, end: IPoint) => {
  return [start.x, start.y, end.x, end.y];
};

const LinkCanvas: React.FC<LinkCanvasProps> = ({ points, lines }) => {
  return (
    <>
      {lines.map((line, index) => {
        const start = points[line.start];
        const end = points[line.end];

        if (line.type === 'line') {
          return (
            <Line
              key={index}
              points={drawLine(start, end)}
              stroke="black"
              strokeWidth={2}
            />
          );
        } else if (line.type === 'curve' && line.depth !== undefined) {
          return (
            <Line
              key={index}
              points={drawCurve(start, end, line.depth, line.offset || 0)}
              stroke="black"
              strokeWidth={2}
              tension={0.5}
              bezier
            />
          );
        }
        return null;
      })}
    </>
  );
};

export default LinkCanvas;
