import  { useCallback, useState } from 'react'
interface pingFormProps {
  onClick : (data:any)=>void;
}
export default function PingForm(props:pingFormProps) {
    const {onClick} = props;
    const [ping_,setPing]=useState<string>(`0`);
    const hanldeChange = useCallback ((e)=>{
        setPing(e.currentTarget.value);      
       },[])
     const handleSubmit = useCallback ((e)=>{
      e.preventDefault();
      onClick(ping_);
     },[ping_])
  return (
    <form onSubmit={handleSubmit} >
            <input 
              value={ping_}  placeholder="0" className="w-11 outline-none text-xs"  
              onChange={hanldeChange}
            />
            <button type='button'></button>
    </form>
  )
}
