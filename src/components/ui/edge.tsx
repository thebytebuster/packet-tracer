
import { Vector2d } from "konva/lib/types";

import { Group,Text, Line} from "react-konva";

const degrees_to_radians = (degrees: number) => degrees * (Math.PI/180);

export interface  EdgeProps {
    node1 :Vector2d ,
    node2 : Vector2d,
    id:string,
    ping:number,
}
export default function Edge(props:EdgeProps){
    const {node1,node2,id,ping} = props;
    console.log({node1,node2,id,ping} );
    
    const dx = node1.x - node2.x;
    const dy = node1.y - node2.y;
    const angle = Math.atan2(-dy, dx);
      
    const radius = 20
    
    const arrowStart = {
      x: node2.x + -radius * Math.cos(angle + Math.PI),
      y: node2.y + radius * Math.sin(angle + Math.PI)
    };
  
    const arrowEnd = {
      x: node1.x + -radius * Math.cos(angle),
      y: node1.y + radius * Math.sin(angle)
    };
  
    const arrowMiddle = {
      x: (arrowStart.x + arrowEnd.x) / 2,
      y: (arrowStart.y + arrowEnd.y) / 2
    };
  
    const text = '0';

    const textPosition = {
      x: arrowMiddle.x - text.length  * Math.sin(degrees_to_radians(90)),
      y: arrowMiddle.y - text.length  * Math.sin(degrees_to_radians(90))
    };
      
    return (
      <Group>
        <Line
          id={id}
          points={[
            arrowStart.x+50,
            arrowStart.y+50,
            arrowEnd.x+50,
            arrowEnd.y+50
          ]}
          stroke="#000"
          fill="#000"
          strokeWidth={2}
          pointerWidth={6}
          
        />
            <Text fill='red' x={textPosition.x} y ={textPosition.y} text={`${ping}`}/>
      </Group>
    );
}