import {  IServer } from "@/type/server";
import { KonvaEventObject } from "konva/lib/Node";
import { Vector2d } from "konva/lib/types";
import {  useCallback, useEffect, useRef, useState } from "react";
import { Group, Image as KonvaImage } from "react-konva";
import { Html } from "react-konva-utils";
import useImage from 'use-image';
import Terminal from "../features/terminal";
import useWorker from "@/hooks/worker.hooks";
import {LinkIcon} from 'lucide-react'
export interface ServerProps 
 extends React.ComponentProps<typeof KonvaImage>,IServer{
    onClickServer : ( args:{pos:Vector2d,ip:string})=>void
    onDragAndMove : (args:{pos:Vector2d,ip:string})=>void
}
export default function  Server(props:ServerProps){
    const {domaine,ip,srcImage,neighbor=[],height,width,position,onClickServer,onDragAndMove,status,...rest} = props;
    const  [image_] = useImage(srcImage);
    const [terminalShow,setTerminalShow] = useState<boolean>(false);
    const {sendMessage,response} = useWorker<string>();
    const groupRef = useRef(null);
    const handleDrageMove = (e:KonvaEventObject<globalThis.DragEvent>)=>{
        const x = e.target.x() 
        const y = e.target.y()
        onDragAndMove({pos:{x,y},ip})
        // setPosition(e.target.getStage().getPointerPosition())
    }
    const hanldeClickServer = (e:any)=>{
        onClickServer({pos:{x:groupRef.current.x(),y:groupRef.current.y()},ip})
    } 
    const onsendMessage = useCallback((e:any)=>{
        sendMessage('send')
    },[])
    useEffect(()=>{
        console.log('response',response);
    },[response]);
    return (
       <Group
            ref={groupRef}
            onDragMove={(e)=>handleDrageMove(e)}
            draggable={true}
            id={ip}
            onDblClick={()=>setTerminalShow(true)}
            {...position}
       >
        <Html
            divProps={{style:{
                top:`-${(width as number)/8}px`,
                left:'-10px',
                display:'flex',
                alignItems:'center',
                justifyContent:'start',
                gap:'4px'
            }}}
        >
         
            <LinkIcon  size={10} onClick={hanldeClickServer} className=" cursor-pointer"/>
            <p className="w-[100px] text-xs text-blue-800">
                {domaine}
            </p>   
        </Html>

         <KonvaImage
            id={ip}
            {...rest}
            image={image_}
            width={height}
            height={width}
        />
        {
            terminalShow &&  (
                <Html
                divProps={{
                    style:{
                        left:`${width as number}px`,
                        top:`-${200}px`
                    } 
                }}
                >
                    <Terminal toogleTerminalShow={()=>setTerminalShow(false)} user={ip} />
                </Html>
            )
        }
        
       </Group>
    )
}