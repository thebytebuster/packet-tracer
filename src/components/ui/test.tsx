import Konva from 'konva';
import { Vector2d } from 'konva/lib/types';
import  { useRef, useEffect } from 'react';
import { Image} from 'react-konva';
import useImage from 'use-image';
interface MyRectProps {
    to : Vector2d,
    from : Vector2d
}
export const MyRect = (props:MyRectProps) => {
  console.log("props",props)
  const rectRef = useRef<any>(null);
  const [image] = useImage("images/packet.png")
  useEffect(() => {
    if(rectRef){
        console.log(rectRef)
        var tween = new Konva.Tween({
            node: (rectRef as any).current,
            duration: 1,
            x: props.to.x,
            y: props.to.y,
            rotation: Math.PI * 2,
            opacity: 1,
            strokeWidth: 6,
            visible:false
          });
        
          // start tween after 2 seconds
          setTimeout(function () {
            tween.play();
          }, 2000);
    }
  }, [rectRef]); // Re-run effect if changeSize changes

  return (
    <Image
          image={image} 
          ref={rectRef}
          width={30}
          height={30}
          {...props.from}
          fill="green"
          draggable    
        />
  );
};