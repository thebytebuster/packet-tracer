import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import { zodResolver } from "@hookform/resolvers/zod";
import { useForm, useWatch } from "react-hook-form";
import { z } from "zod";
import { CirclePlus } from "lucide-react";
import {
  Sheet,
  SheetClose,
  SheetContent,
  SheetDescription,
  SheetFooter,
  SheetHeader,
  SheetTitle,
  SheetTrigger,
} from "@/components/ui/sheet";
import {
  FormField,
  FormItem,
  Form,
  FormLabel,
  FormControl,
  FormMessage,
  FormDescription,
} from "../ui/form";
import { useCallback } from "react";
import useDrawStore from "@/store/draw.store";
import { Select, SelectContent, SelectItem, SelectTrigger, SelectValue } from "../ui/select";

const formSchema = z.object({
  x: z.number().min(1, {
    message: "must positive",
  }),
  y: z.number().min(1, {
    message: "must positive",
  }),
  label: z.string(),
  end: z.number(),
  type: z.string(),
  start: z.number(),
  depth: z.string(),
  point: z.string()
});
export function SheetCanvas() {
  const points = useDrawStore().points;
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      x: 0,
      y: 0,
      label: "",
      end: 0,
      type: ''
    }
  });
  const addPoint = useDrawStore().addPoint;
  const addLine = useDrawStore().addLine;
  const handleSubmit = useCallback(() => {
    const value = form.getValues();
    if(value.point === "link a point" ){
      addLine({
        type:value.type,
        start:+value.start,
        end:+value.end,
        depth:+value.depth
      })
    }else{
      addPoint({
        x: +form.getValues().x + 100,
        y: (+form.getValues().y) + 100,
        label: form.getValues().label
      })
    }
    form.reset();
  }, []);
  const type = useWatch({ control: form.control, name: 'type' });
  const point = useWatch({ control: form.control, name: 'point' });


  return (
    <Sheet>
      <SheetTrigger asChild>
        <Button variant="outline" className="flex gap-x-3">
          <span>add new</span> <CirclePlus />
        </Button>
      </SheetTrigger>
      <SheetContent>
        <SheetHeader>
          <SheetTitle>Draw line and Curve</SheetTitle>
        </SheetHeader>
        <Form {...form}>
          <form className="grid gap-3">

            <FormField
              control={form.control}
              name="point"
              render={({ field }) => (
                <FormItem>
                  <FormLabel>Point</FormLabel>
                  <Select onValueChange={(e) => { field.onChange(e) }} defaultValue={field.value}>
                    <FormControl>
                      <SelectTrigger>
                        <SelectValue placeholder="create or connect point" />
                      </SelectTrigger>
                    </FormControl>
                    <SelectContent>
                      <SelectItem value='create point' > Create point </SelectItem>
                      <SelectItem value='link a point'>  Link a point</SelectItem>
                    </SelectContent>
                  </Select>
                  <FormMessage />
                </FormItem>
              )}
            />
            {
              point == 'create point' && (
                <div className="grid grid-cols-2 gap-2 border p-2">
                  <FormField
                    control={form.control}
                    name="x"
                    render={({ field }) => (
                      <FormItem>
                        <FormLabel>X</FormLabel>
                        <FormControl>
                          <Input placeholder="X coordinate" {...field} />
                        </FormControl>
                        <FormMessage />
                      </FormItem>
                    )}
                  />
                  <FormField
                    control={form.control}
                    name="y"
                    render={({ field }) => (
                      <FormItem>
                        <FormLabel>Y</FormLabel>
                        <FormControl>
                          <Input placeholder="Y coordinate" {...field} />
                        </FormControl>
                        <FormMessage />
                      </FormItem>
                    )}
                  />
                  <FormField
                    control={form.control}
                    name="label"
                    render={({ field }) => (
                      <FormItem>
                        <FormLabel>Label</FormLabel>
                        <FormControl>
                          <Input placeholder="Label (example A,B..)" {...field} />
                        </FormControl>
                        <FormMessage />
                      </FormItem>
                    )}
                  />
                </div>
              )
            }
            {
              point === 'link a point' && (

                <>
                  <div className="grid grid-cols-2 gap-2">
                  <FormField
                    control={form.control}
                    name="start"
                    render={({ field }) => (
                      <FormItem>
                        <FormLabel>Depart</FormLabel>
                        <Select onValueChange={field.onChange}>
                          <FormControl>
                            <SelectTrigger>
                              <SelectValue placeholder="Select a start point" />
                            </SelectTrigger>
                          </FormControl>
                          <SelectContent>
                            {
                              points.map((point, idx) => {
                                return <SelectItem value={`${idx}`} > {point.label} </SelectItem>
                              })
                            }
                          </SelectContent>
                        </Select>
                        <FormMessage />
                      </FormItem>
                    )}
                  />
                  <FormField
                    control={form.control}
                    name="end"
                    render={({ field }) => (
                      <FormItem>
                        <FormLabel>Destination</FormLabel>
                        <Select onValueChange={field.onChange}>
                          <FormControl>
                            <SelectTrigger>
                              <SelectValue placeholder="Select a end point" />
                            </SelectTrigger>
                          </FormControl>
                          <SelectContent>
                            {
                              points.map((point, idx) => {
                                return <SelectItem value={`${idx}`} > {point.label} </SelectItem>
                              })
                            }
                          </SelectContent>
                        </Select>
                        <FormMessage />
                      </FormItem>
                    )}
                  />
                  </div>
                  <FormField
                    control={form.control}
                    name="type"
                    render={({ field }) => (
                      <FormItem>
                        <FormLabel>Type Link</FormLabel>
                        <Select onValueChange={(e) => { field.onChange(e) }} defaultValue={field.value}>
                          <FormControl>
                            <SelectTrigger>
                              <SelectValue placeholder="select the type link (line or curve)" />
                            </SelectTrigger>
                          </FormControl>
                          <SelectContent>
                            <SelectItem value='curve' > Curve </SelectItem>
                            <SelectItem value='line' > Line </SelectItem>
                          </SelectContent>
                        </Select>
                        <FormMessage />
                      </FormItem>
                    )}
                  />
                  {
                    type === 'curve' &&
                    (<FormField
                      control={form.control}
                      name="depth"
                      render={({ field }) => (
                        <FormItem>
                          <FormLabel>Depth of courbe</FormLabel>
                          <FormControl>
                            <Input placeholder="Depth" {...field} />
                          </FormControl>
                          <FormMessage />
                        </FormItem>
                      )}
                    />)
                  }
                </>

              )
            }

          </form>
        </Form>
        <SheetFooter>
          <SheetClose asChild>
            <Button type="submit" onClick={handleSubmit} className="my-2">
              Save changes
            </Button>
          </SheetClose>
        </SheetFooter>
      </SheetContent>
    </Sheet>
  );
}
