import React, { useState } from 'react';

interface TerminalProps {
    user : string
    toogleTerminalShow : ()=>void
}

export default function Terminal(props:TerminalProps) {
  const {toogleTerminalShow,user} = props;
  const [input, setInput] = useState<string>('');
  const [output, setOutput] = useState<string>('');

  const commands: Record<string, () => string> = {
    hello: () => "Hello, world!",
    date: () => new Date().toLocaleString(),
  };

  const executeCommand = (command: string): void => {
    if (commands[command]) {
      setOutput(commands[command]());
    } else {
      setOutput(`Unknown command: ${command}`);
    }
  };

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
    setInput(event.target.value);
  };

  const handleEnterKey = (event: React.KeyboardEvent<HTMLInputElement>): void => {
    if (event.key === 'Enter') {
      executeCommand(input);
      setInput(''); 
    }
  };

  return (
    <div className="flex h-[200px] w-full max-w-[500px] flex-col rounded-lg bg-[#1e1e1e] shadow-lg">
      <div className="flex h-5 items-center justify-between rounded-t-lg bg-[#252525] px-4 text-sm text-gray-400">
      <div className="flex items-center gap-2">
          <button className="h-2 w-2 rounded-full bg-[#ff5f57]" onClick={()=>toogleTerminalShow()} />
          <button className="h-2 w-2 rounded-full bg-[#febc2e]" />
          <button className="h-2 w-2 rounded-full bg-[#28c840]" />
        </div>
      </div>
      <div className="flex-1 overflow-auto px-4 py-2 text-[#c5c5c5] text-xs">
        <div className="flex items-baseline">
          <span className="mr-2 font-medium text-[#9cdcfe]">{user}@host</span>
          <span className="mr-2 text-[#ce9178]">~</span>
          <span className="mr-2 font-medium text-[#dcdcaa]">$</span>
          <input
            autoFocus
            className="w-full bg-transparent text-[#c5c5c5] outline-none"
            type="text"
            value={input}
            onChange={handleInputChange}
            onKeyDown={handleEnterKey}
          />
          {/* <div className="h-5 w-1 animate-blink rounded bg-[#c5c5c5]" /> */}
        </div>
        {/* Output area */}
        <div className="mt-4">
          {output}
        </div>
      </div>
    </div>
  );
};

