import React, { ReactNode, useCallback } from 'react'
import { Dialog, DialogContent, DialogDescription, DialogFooter, DialogHeader, DialogTitle, DialogTrigger } from '../ui/dialog'
import { Button } from '../ui/button'

interface BaseDialogProps{
  children : ReactNode,
  title:string,
  description?:string,
  open: boolean;
  onClose: () => void;
}
export default function BaseDialog(
  {children,title,description,open,onClose}:BaseDialogProps)
   {
    const handleClose = useCallback(() => {
      onClose();
      //handleClose()
    }, [onClose]) 
  return (
    <Dialog
    onOpenChange={handleClose}
    open={open}
    >
      {/* <DialogTrigger asChild>
        <Button variant="outline">Edit Profile</Button>
      </DialogTrigger> */}
      <DialogContent className="sm:max-w-[425px]">
        <DialogHeader>
          <DialogTitle>{title}</DialogTitle>
          <DialogDescription>
            {description}
          </DialogDescription>
        </DialogHeader>
          {children}
        <DialogFooter>
          <Button type="submit">Save changes</Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  )
}
