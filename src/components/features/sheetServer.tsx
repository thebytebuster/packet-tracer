import { Button } from "@/components/ui/button"
import { zodResolver } from "@hookform/resolvers/zod"
import { useForm } from "react-hook-form"
import { z } from "zod"
import { 
  Sheet,
  SheetContent,
  SheetDescription,
  SheetHeader,
  SheetTitle,
} from "@/components/ui/sheet"
import { FormField,Form} from "../ui/form";
import { IServer } from "@/type/server"
import { Select, SelectContent, SelectGroup, SelectItem, SelectTrigger, SelectValue } from "../ui/select"
import { useCallback, useMemo } from "react"
import { useServerStore } from "@/store/server.store"
const   domains :Partial<IServer>[]= [
  { domaine: "example1.com", ip: "192.168.1.1" },
  { domaine: "example2.net", ip: "192.168.1.2" },
  { domaine: "example3.org", ip: "192.168.1.3" },
  { domaine: "example4.info", ip: "192.168.1.4" },
  { domaine: "example5.biz", ip: "192.168.1.5" },
  { domaine: "example6.co", ip: "192.168.1.6" },
  { domaine: "example7.us", ip: "192.168.1.7" },
  { domaine: "example8.uk", ip: "192.168.1.8" },
  { domaine: "example9.de", ip: "192.168.1.9" },
  { domaine: "example10.fr", ip: "192.168.1.10" },
  { domaine: "example11.es", ip: "192.168.1.11" },
  { domaine: "example12.it", ip: "192.168.1.12" },
  { domaine: "example13.nl", ip: "192.168.1.13" },
  { domaine: "example14.se", ip: "192.168.1.14" },
  { domaine: "example15.no", ip: "192.168.1.15" },
  { domaine: "example16.fi", ip: "192.168.1.16" },
  { domaine: "example17.dk", ip: "192.168.1.17" },
  { domaine: "example18.pl", ip: "192.168.1.18" },
  { domaine: "example19.ch", ip: "192.168.1.19" },
  { domaine: "example20.jp", ip: "192.168.1.20" }
];
const formSchema = z.object({
    domaine: z.string().min(2, {
      message: "Username must be at least 2 characters.",
    }),
    ip : z.string().min(2,{
        message:'ip invalid'
    })
  })

interface sheetServerProps {
  open: boolean;
  onClose: () => void;
}
export function SheetServer(props:sheetServerProps) {
  const {addServer} = useServerStore();
  const {open,onClose} = props;
    const form = useForm<z.infer<typeof formSchema>>({
        resolver: zodResolver(formSchema),
        defaultValues: {
            domaine: "",
        },
      });
    function onSubmit(values: z.infer<typeof formSchema>) {
      addServer({
        position: { "x":0, "y":0 },
        neighbor: [],
        srcImage: "images/server_on.svg",
        ...values,
        status: true
      });
      form.reset();
      onClose();
    }
    const domaineValue = form.watch('domaine');
    const ipValue = form.watch('ip');
    const [domainsFilter] = useMemo(() => {
      if(!domaineValue && !ipValue ){
        return [domains]
      }
      const res = domains.filter((d) => {
        return d.domaine === domaineValue || d.ip === ipValue;
      });
      return [res];
    }, [domaineValue,ipValue]);   
    const handleClose = useCallback(() => {
      onClose();
    }, [onClose]) 
  return (
    <Sheet open={open} onOpenChange={handleClose} >
      {/* <SheetTrigger asChild>
        <Button variant="outline" className="flex gap-x-3">
            <span>add new</span> <CirclePlus />
        </Button>
      </SheetTrigger> */}
      <SheetContent>
      <SheetHeader>
        <SheetTitle>Servers</SheetTitle>
        <SheetDescription>
          Make changes to your profile here. Click save when you're done.
        </SheetDescription>
      </SheetHeader>
      <Form {...form}>

        <form onSubmit={form.handleSubmit(onSubmit)} className="flex  flex-col gap-3" >
            <FormField
                  control={form.control}
                  name="domaine"
                  render={({ field }) => (
                    <Select   {...field} onValueChange={(value)=>field.onChange(value)}>
                      <SelectTrigger className="w-full">
                        <SelectValue placeholder="Domaine" className="text-sm" />
                      </SelectTrigger>
                      <SelectContent>
                        <SelectGroup>
                          {
                            domainsFilter ? domainsFilter.map((D,idx)=>{
                              return  <SelectItem  key={idx} value={D.domaine as string}>{D.domaine}</SelectItem>
                            }) : domains.map((D,idx)=>{
                              return  <SelectItem  key={idx} value={D.domaine as string}>{D.domaine}</SelectItem>
                            })
                          }
                        </SelectGroup>
                      </SelectContent>
                  </Select>
                )}
              />
              <FormField
                control={form.control}
                name="ip"
                render={({ field }) => (
                  <Select   {...field} onValueChange={(value)=>field.onChange(value)}>
                    <SelectTrigger className="w-full">
                      <SelectValue placeholder="IP adress" className="text-sm" />
                    </SelectTrigger>
                    <SelectContent>
                      <SelectGroup>
                        {
                          domainsFilter ? domainsFilter.map((D,idx)=>{
                            return  <SelectItem  key={idx} value={D.ip as string}>{D.ip}</SelectItem>
                          })  :  domains.map((D,idx)=>{
                            return  <SelectItem  key={idx} value={D.ip as string}>{D.ip}</SelectItem>
                          })
                        }
                      </SelectGroup>
                    </SelectContent>
              </Select>
                )}
              />
               <Button type="submit" className="my-2">Save changes</Button>
        </form>
      </Form>
</SheetContent>

    </Sheet>
  )
}
