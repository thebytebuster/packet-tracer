import { INeighbor, IServer } from "@/type/server";

const neighbors: INeighbor[] = [
    { ip: "192.168.1.1", ping: "12ms" },
    { ip: "192.168.1.2", ping: "15ms" },
    { ip: "192.168.1.3", ping: "20ms" }
];

// Define mock data for the IServer interface using the neighbors array
export const servers: IServer[] = [
    {
        domaine: "example.com",
        ip: "192.168.1.100",
        srcImage: "images\server1.svg",
        neighbor: neighbors
    },
    {
        domaine: "testsite.com",
        ip: "192.168.1.101",
        srcImage: "images\server1.svg",
        neighbor: [
            { ip: "192.168.1.4", ping: "10ms" },
            { ip: "192.168.1.5", ping: "22ms" }
        ]
    },
    {
        domaine: "mywebsite.net",
        ip: "192.168.1.102",
        srcImage: "images\server1.svg",
        neighbor: [
            { ip: "192.168.1.6", ping: "18ms" }
        ]
    }
];