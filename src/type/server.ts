import { Vector2d } from "konva/lib/types";
export type IConnector = {
    from: Vector2d & {id:string}; // Assuming 'from' and 'to' are IDs of shapes or servers
    to: Vector2d & {id:string};
    id: string; // Unique identifier for the connector
    ping:number
  };
export interface INeighbor {
   ip : string;
   ping:number; 
}

export interface IServer {
    domaine:string;
    ip:string;
    srcImage:string;
    neighbor:INeighbor[],
    status:boolean,
    position:Vector2d
}
export enum DialogType {
  'CREATESERVER',
  'DIKSTRA'
}