
export interface IPoint {
    x: number;
    y: number;
    label: string;
  }
  
export interface ILine {
    type: 'line' | 'curve';
    start: number;
    end: number;
    depth?: number;
    offset?: number;
  }