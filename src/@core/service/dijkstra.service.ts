import { IServer } from "@/type/server";

type Graph = Record<string, Record<string, number>>;
type SiteList = Record<string, string[]>;
type Statut = Record<
  string,
  {
    visited: boolean;
    actif: boolean;
    value: number;
    previous: null | string;
  }
>;
export class DijkstraService {
  dijkstra(servers: IServer[], startNode: string, endNode: string): { path: { node: string, weight: number }[] } | null {
    const graph = this.transformData(servers);
    const distances: Record<string, number> = {};
    const previousNodes: Record<string, string | null> = {};

    // Initialize distances with Infinity for all nodes except the start node
    Object.keys(graph).forEach(node => {
      distances[node] = node === startNode? 0 : Infinity;
      previousNodes[node] = null;
    });

    let unvisitedNodes = [...Object.keys(graph)];

    while (unvisitedNodes.length > 0) {
      // Find the node with the smallest distance
      let currentNode = unvisitedNodes.reduce((a, b) => distances[a] < distances[b]? a : b);

      // If the smallest distance is Infinity, we've found the shortest path to all nodes
      if (distances[currentNode] === Infinity) break;

      unvisitedNodes = unvisitedNodes.filter(node => node!== currentNode);

      // Update distances to neighboring nodes
      const neighbors = graph[currentNode];
      Object.keys(neighbors).forEach(neighbor => {
        const altDistance = distances[currentNode] + neighbors[neighbor];
        if (altDistance < distances[neighbor]) {
          distances[neighbor] = altDistance;
          previousNodes[neighbor] = currentNode;
        }
      });
    }

    // Reconstruct the path from startNode to endNode and calculate its weight for each node
    if (distances[endNode]!== Infinity) {
      const path: { node: string, weight: number }[] = [];
      let currentNode = endNode;
      while (currentNode!== null) {
        path.unshift({ node: currentNode, weight: distances[currentNode] }); // Add the node and its weight to the path
        currentNode = previousNodes[currentNode] as string;
      }
      return { path }; // Return the path with each node's weight
    } else {
      return null; // No path found
    }
  }

  transformData(servers: IServer[]): Graph {
    const graph: any = {};

    servers.forEach(server => {
      const neighbors = server.neighbor?.reduce((acc: any, neighbor) => {
        acc[neighbor.ip] = neighbor.ping;
        return acc;
      }, {});

      graph[server.ip] = neighbors;
    });
    return graph;
  }
}
